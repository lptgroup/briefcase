/*ADMIN*/
var adminRoutes = FlowRouter.group({
  prefix: '/admin',
  triggersEnter: [function(context, redirect) {
    console.log('running group triggers');
  }]
});

adminRoutes.route('/', {
  name: 'dashboard',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'Dashboard'});
  },
  triggersEnter: [function(context, redirect) {
    console.log('running /admin trigger');
  }]
});

adminRoutes.route('/file', {
  name: 'file',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'myForm'});
  }
});

adminRoutes.route('/users', {
  name: 'users',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'UsersList'});
  }
});
adminRoutes.route('/users/create', {
  name: 'createUser',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'createUserForm'});
  },
  triggersEnter: [function(context, redirect) {
    console.log('running /users/new trigger');
  }]
});

adminRoutes.route('/presentations', {
  name: 'presentationsList',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'PresentationsList'});
  }
});
adminRoutes.route('/presentations/create', {
  name: 'createPresentation',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'createPresentationForm'});
  }
});
adminRoutes.route('/presentation/:id', {
  name: 'presentation',
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'PresentationSingle'});
  }
});

adminRoutes.route('/presentations/new', {
  action: function() {
    BlazeLayout.render('AdminLayout', {main: 'NewPresentation'});
  }
});
