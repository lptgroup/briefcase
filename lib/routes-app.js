if (Meteor.isClient) {
    Accounts.onLogin(function(){
        FlowRouter.go('briefcase');
    });

    Accounts.onLogout(function(){
        FlowRouter.go('home');
    });
}

FlowRouter.triggers.enter([function(context, redirect){
    if (!Meteor.userId()) {
        FlowRouter.go('home');
    }
}]);

FlowRouter.route('/', {
   name: 'home',
   action() {
        if (Meteor.userId()) {
            FlowRouter.go('briefcase');
        }
        BlazeLayout.render('HomeLayout');
    }
});

FlowRouter.route('/teczka', {
    name: 'briefcase',
    action() {
        BlazeLayout.render('MainLayout', {main:'Menu'});
    }
});

FlowRouter.route('/agenda', {
    name: 'agenda',
    action() {
        BlazeLayout.render('MainLayout', {main:'Agenda'});
    }
});

FlowRouter.route('/prezentacje', {
    name: 'presentations',
    action() {
        BlazeLayout.render('MainLayout', {main:'Presentations'});
    }
});

FlowRouter.route('/notes', {
    name: 'notes',
    action() {
        BlazeLayout.render('MainLayout', {main:'Notes'});
    }
});

FlowRouter.route('/informacje-logistyczne', {
    name: 'logistics',
    action() {
        BlazeLayout.render('MainLayout', {main:'Logistics'});
    }
});

