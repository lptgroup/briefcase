Template.myForm.events({
  'change .myFileInput': function(event, template) {
    var files = event.target.files;
    console.log(files);
    for (var i = 0, ln = files.length; i < ln; i++) {
      Images.insert(files[i], function (err, fileObj) {
      	console.log(err);
      	console.log(fileObj);
        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
      });
    }
  }
});

Template.imageView.helpers({
  images: function () {
    return Images.find(); // Where Images is an FS.Collection instance
  }
});