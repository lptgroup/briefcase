if (Meteor.isClient) {
	Template.createUserForm.helpers({
    schema: function () {
        return Schema.createUserFormSchema;
    }
  });
}