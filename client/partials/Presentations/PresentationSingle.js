Template.PresentationSingle.onCreated(function(){
    var self = this;
    self.autorun(function(){
    	var id = FlowRouter.getParam('id');
        self.subscribe('singlePresentation', id);
    });
});

Template.PresentationSingle.helpers({
  presentation:()=> {
    var id = FlowRouter.getParam('id');
    return Presentations.findOne({_id: id});
	}
});

Template.ImageSingle.helpers({
  imageSingle: function () {
  	console.log( 5 );
  	return Images.findOne({_id: this.src});
  }
});