Template.LoginForm.events({

  'submit #LoginForm' : function(e, t){
    e.preventDefault();
    // retrieve the input field values
    var email = t.find('#LoginForm__email').value,
        password = t.find('#LoginForm__password').value;

      // Trim and validate your fields here.... 
			var email = trimInput(email);
		
      Meteor.loginWithPassword(email, password, function(err){
        if (err){
          sweetAlert(   'Błąd logowania!',   'Brak użytkownika lub niepoprawne hasło.',   'error' );
        }else{
        	
        }
      });
      return false; 
    }
});

// trim helper
var trimInput = function(val) {
  return val.replace(/^\s*|\s*$/g, "");
};
