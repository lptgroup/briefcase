Template.AdminLayout.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('userData');
        self.subscribe('presentations');
        self.subscribe('images');
    });
});

Template.AdminLayout.onRendered(function () {
    var self = this;
    if (self.view.isRendered) {
        var body = $('body');
            body.removeClass();
            body.addClass("skin-blue sidebar-mini fixed");

        $(function () {
            MeteorAdminLTE.run();
        });
    }
});