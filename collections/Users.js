Schema = {};
Schema.UserProfile = new SimpleSchema({
    firstName: {
        type: String,
        label: "First Name",
        optional: true
    },
    lastName: {
        type: String,
        label: "Last Name",
        optional: true
    },
});
Schema.createUserFormSchema = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    password: {
        type: String
        // create a regex here to restrict password to a format
    },
    passwordConfirmation: {
        type: String,
        // this is a custom validation to ensure the password match
        custom: function () {
            if (this.value !== this.field('password').value) {
                return ("passwordMismatch");
            }
        }
    },
    profile: {
        type: Schema.UserProfile,
        optional: true
    }
});

/*
 * custom errors message for autoform
 * we use it for the error 'passwordMismatch', since it is a
 * custom validation and autoform have no predefined messages for it
 */

Schema.createUserFormSchema.messages({
    "passwordMismatch": "Passwords do not match",
});



