Presentations = new Mongo.Collection('presentations');

Presentations.allow({
    insert: function(userId, doc){
        return !!userId;
    },
    update: function(userId, doc){
        return !!userId;
    }
});

Slide = new SimpleSchema({
    src: {
        type: String,
        autoform: {
            type: "fileUpload",
            collection: 'Images',
            label: 'Choose file'
        }
    },
    isActive: {
        type: Boolean,
        defaultValue: false,
        autoform: {
            type: "hidden"
        }
    },
    isCurrent: {
        type: Boolean,
        defaultValue: false,
        autoform: {
            type: "hidden"
        }
    }
});

PresentationSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Tytuł"
    },
    author: {
        type: String,
        label: "Autor"
    },
    slides: {
        type: [Slide]
    },
    isActive: {
        type: Boolean,
        defaultValue: false,
        autoform: {
            type: "hidden"
        }
    },
    createdAt: {
        type: Date,
        label: "Created At",
        autoValue: function(){
            return new Date()
        },
        autoform: {
            type: "hidden"
        }
    }
});

Meteor.methods({
    togglePresentationActive: function(id, currentState){
        Presentations.update(id, {
            $set: {
                isActive: !currentState
            }
        });
    },
    toggleSlideActive: function(id, currentState){
        Slides.update(id, {
            $set: {
                isActive: !currentState
            }
        });
    },
    setCurrent: function(id){
        Slides.update({}, {
            $set: {
                iScurrent: false
            }
        });
        Slides.update(id, {
            $set: {
                iScurrent: true
            }
        });
    }
});

Presentations.attachSchema( PresentationSchema );