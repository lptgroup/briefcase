Meteor.publish('presentations', function(){
    return Presentations.find({});
});

Meteor.publish('singlePresentation', function(id){
	check(id, String);
	return Presentations.find({_id: id});
});

Meteor.publish('images', function(){
    return Images.find({});
});
Meteor.publish('singleImage', function(id){
	check(id, String);
	return Images.find({_id: id});
});

Meteor.publish("userData", function () {
    return Meteor.users.find({}, {
    	fields: {
    		username: 1,
    		emails: 1,
    		profile: 1
    	}
    });
});

// Meteor.publish('current', function(){
// 	return Slides.find({isCurrent: true});
// })