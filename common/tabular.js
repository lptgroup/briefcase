if (!this.TabularTables) {
  this.TabularTables = {};
}

Meteor.isClient && Template.registerHelper("TabularTables", TabularTables);

TabularTables.Users = new Tabular.Table({
  name: "Users",
  collection: Meteor.users,
  columns: [
    {
      data: "emails[0].address",
      title: "Email"
    }, {
      data: "profile.firstName",
      title: "firstName"
    }, {
      data: "profile.lastName",
      title: "lastName"
    }
  ]
});

TabularTables.Presentations = new Tabular.Table({
  name: "Presentations",
  collection: Presentations,
  columns: [
    {
      data: "name",
      title: "Tytuł"
    }, {
      data: "author",
      title: "Autor"
    }
  ]
});